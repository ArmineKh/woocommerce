<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function woocommercecustomtheme_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'woocommercecustomtheme' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'woocommercecustomtheme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'woocommercecustomtheme_widgets_init' );


/**
* Custom shop sidebar
*/
function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'shop-sidebar', 'woocommercecustomtheme' ),
            'id' => 'shop-side-bar',
            'description' => __( 'Custom Sidebar for shop page', 'woocommercecustomtheme' ),
            'before_widget' => '<div class="widget-content container">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar' );