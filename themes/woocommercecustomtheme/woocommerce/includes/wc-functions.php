<?php 
function get_all_categories_for_slider(){
	$taxonomy     = 'product_cat';
	$orderby      = 'name';  
	$show_count   = 0;      
	$pad_counts   = 0;  
	$hierarchical = 1;   
	$title        = '';  
	$empty        = 0;

	$args = array(
		'taxonomy'     => $taxonomy,
		'orderby'      => $orderby,
		'show_count'   => $show_count,
		'pad_counts'   => $pad_counts,
		'hierarchical' => $hierarchical,
		'title_li'     => $title,
		'hide_empty'   => $empty
	);
	return get_categories( $args );
}

function get_all_categories_by_name_for_header(){

	$taxonomy     = 'product_cat';
	$orderby      = 'name';  
	$show_count   = 0;      
	$pad_counts   = 0;  
	$hierarchical = 1;   
	$title        = '';  
	$empty        = 0;

	$args = array(
		'taxonomy'     => $taxonomy,
		'orderby'      => $orderby,
		'show_count'   => $show_count,
		'pad_counts'   => $pad_counts,
		'hierarchical' => $hierarchical,
		'title_li'     => $title,
		'hide_empty'   => $empty
	);
	return get_categories( $args );
}

// single product page functions
add_action('woocommerce_before_single_product', 'product_details_wrapper_start', 5);
function product_details_wrapper_start(){
	?>
	<div class="container">
		<div class="row product-details">
			

			<?php
		}

		add_action('woocommerce_after_single_product', 'product_details_wrapper_end', 5);
		function product_details_wrapper_end(){
			?>
		</div>
	</div>


	<?php
}


add_action('woocommerce_before_single_product_summary', 'product_details_wrapper_image_start', 5);
function product_details_wrapper_image_start(){
	?>
	<div class="col-lg-6 col-md-6">
		<div class="product__details__pic">
			

			<?php
		}

		add_action('woocommerce_before_single_product_summary', 'product_details_wrapper_image_end', 25);
		function product_details_wrapper_image_end(){
			?>
		</div>
	</div>

	<?php
}
add_action('woocommerce_before_single_product_summary', 'product_details_wrapper_text_start', 30);
function product_details_wrapper_text_start(){
	?>
	<div class="col-lg-6 col-md-6 float-right">
		<div class="product__details__text">

			<?php
		}

		add_action('woocommerce_after_single_product_summary', 'product_details_wrapper_text_end', 5);
		function product_details_wrapper_text_end(){
			?>
		</div>
	</div>

	<?php
}


add_action('woocommerce_before_quantity_input_field', 'add_custom_quantity_start', 10);
function add_custom_quantity_start(){
	?>
	<input class="minus" type="button" value="-">
	<?php
}
add_action('woocommerce_before_quantity_input_field', 'add_custom_quantity_end',20);
function add_custom_quantity_end(){
	?>
	<input class="plus" type="button" value="+">
	<?php
}


add_action('woocommerce_after_single_product_summary', 'add_tabs_and_releted_product', 45);
function add_tabs_and_releted_product(){
	woocommerce_output_product_data_tabs();
	woocommerce_output_related_products();
}

add_filter( 'woocommerce_review_gravatar_size', 'resize_avatar_image', $priority = 10, $accepted_args = 1 );
function resize_avatar_image(){
	return '100';
}
add_filter( 'woocommerce_output_related_products', 'woocommerce_output_related_products', $priority = 10, $accepted_args = 1 );
function woocommerce_output_related_products() {
	woocommerce_related_products(3,3);
}

// shop page functions

add_action('woocommerce_before_main_content', 'shop_page_wrapper_start', 5);
function shop_page_wrapper_start(){
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">


				<?php
			}

			add_action('woocommerce_after_main_content', 'shop_page_wrapper_end', 5);
			function shop_page_wrapper_end(){
				?>
			</div>
		</div>
	</div>


	<?php
}

add_action('woocommerce_before_shop_loop', 'shop_sidebar_wrapper_start', 5);
function shop_sidebar_wrapper_start(){
	?>
	<div class="row">
		<div class="col-lg-3 col-md-5">
			<?php	get_sidebar('shop'); ?>

			<?php
		}

		add_action('woocommerce_before_shop_loop', 'shop_sidebar_wrapper_end', 35);
		function shop_sidebar_wrapper_end(){
			?>

		</div>

		<?php
	}

	add_action('woocommerce_before_shop_loop', 'shop_products_wrapper_start', 40);
	function shop_products_wrapper_start(){
		?>
		<div class="col-lg-9 col-md-7 ">


			<?php
		}

		add_action('woocommerce_after_shop_loop', 'shop_products_wrapper_end', 15);
		function shop_products_wrapper_end(){
			?>

		</div>
	</div>
	<?php
}
/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}


// chechout page functions


function remove_wc_sidebar_conditional( $array ) {

  // Hide sidebar on product pages by returning false
	if ( is_checkout() )
		return false;

  // Otherwise, return the original array parameter to keep the sidebar
	return $array;
}

add_filter( 'is_active_sidebar', 'remove_wc_sidebar_conditional', 10, 2 );


add_action( 'woocommerce_before_checkout_form' , 'add_wrapper_checkout_start', $priority = 5, $accepted_args = 1 );
function add_wrapper_checkout_start(){
	?>
	<div class="container">
		<div class="row">
			

			<?php
		}
		add_action( 'woocommerce_after_checkout_form', 'add_wrapper_checkout_end', $priority = 35, $accepted_args = 1 );
		function add_wrapper_checkout_end(){
			?>
		</div>
	</div>
	<?php
}

add_filter( 'woocommerce_checkout_coupon_message', 'have_coupon_message');

function have_coupon_message() {
	return '<i class="fas fa-ticket-alt" aria-hidden="true"></i> Have a coupon? <a href="#" class="showcoupon">Click here to enter your discount code</a>';
}


// add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// function custom_override_checkout_fields( $fields ) {
// 	echo '<pre>', var_dump($fields), '</pre>';

// 	unset($fields['billing']['billing_country']["required"]);
// 	$fields['billing']['billing_state']["required"] = false;

//  	unset($fields['billing']['billing_country']);
//  	unset($fields['billing']['billing_state']);
// 	echo '<pre>', var_dump($fields), '</pre>';

//     return $fields;
// }