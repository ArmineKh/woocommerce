<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package woocommerceCustomTheme
 */

get_header();?>
<section class="categories">
	<div class="container">
		<div class="row">
			<div class="categories_slider owl-carousel owl-loaded owl-drag">

				<?php
				$all_categories = get_all_categories_for_slider();
				foreach ($all_categories as $cat) {

					$category_id = $cat->term_id; 
					$category_name = $cat->name;

					$cat_thumb_id = get_term_meta( $category_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $cat_thumb_id );

					echo	'<div class="categories_item" >
					<img src="'.$image.'" alt="">
					<h5><a href="'.get_term_link($cat->slug, 'product_cat').'">'.$category_name.'</a></h5>
					</div>';
				}
				?>

			</div>
		</div>
	</div>
</section>

<section class="featured">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="section-title">
					<h2>Featured Product</h2>
				</div>
				<div class="featured_controls">
					<ul class="featured_controls_ul">
						<li class="active" >All</li>
						<li >Oranges</li>
						<li >Fresh Meat</li>
						<li >Vegetables</li>
						<li >Fastfood</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="featured_filter_container">
		<div class="row featured_filter all">
			<?php echo do_shortcode( '[products limit="4" columns="4" class="quick-sale"] ', true ) ?>		
		</div>
		<div class="row featured_filter oranges" style="display:none;">
			<?php echo do_shortcode( '[products limit="4" columns="4" class="quick-sale" category="oranges"] ', true ) ?>
		</div>
		<div class="row featured_filter meat" style="display:none;">
			<?php echo do_shortcode( '[products limit="4" columns="4" class="quick-sale" category="meat"] ', true ) ?>
		</div>
		<div class="row featured_filter vegetables" style="display:none;">
			<?php echo do_shortcode( '[products limit="4" columns="4" class="quick-sale" category="vegetables"] ', true ) ?>
		</div>
		<div class="row featured_filter fastfood" style="display:none;">
			<?php echo do_shortcode( '[products limit="4" columns="4" class="quick-sale" category="fastfood"] ', true ) ?>
		</div>
	</div>
		
	</div>
</section>

<div class="banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="banner_pic">
					<img src="<?php echo get_template_directory_uri().'/assets/img/banner-1.jpg' ?>" alt="">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="banner_pic">
					<img src="<?php echo get_template_directory_uri().'/assets/img/banner-2.jpg' ?>" alt="">
				</div>
			</div>
		</div>
	</div>
</div>

<section class="latest-product">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="latest-product_text ">
					<h4>Latest Products</h4>

					<?php
					$args = array(
						'post_type' => 'product',
						'posts_per_page' => 3,
						'orderby' =>'date',
						'order' => 'DESC' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
						<div class="latest-product_slider owl-carousel " id="latest-product">
							<div class="latest-prdouct_slider_item ">
								<a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<div class="latest-product_item_pic">
										<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="My Image Placeholder" width="65px" height="115px" />'; ?>
									</div>
									<div class="latest-product_item_text">
										<h6><?php the_title(); ?></h6>
										<span><?php echo $product->get_price_html(); ?></span>
									</div>

								</a>

							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="latest-product_text">
					<h4>Top Rated Products</h4>
					<?php
					$args = array(
						'post_type' => 'product',
						'posts_per_page' => 3,
						'orderby' =>'date',
						'order' => 'DESC' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
						<div class="latest-product_slider owl-carousel ">
							<div class="latest-prdouct_slider_item ">
								<a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<div class="latest-product_item_pic">
										<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="My Image Placeholder" width="65px" height="115px" />'; ?>
									</div>
									<div class="latest-product_item_text">
										<h6><?php the_title(); ?></h6>
										<span><?php echo $product->get_price_html(); ?></span>
									</div>
								</a>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="latest-product_text">
					<h4>Top Rated Products</h4>
					<?php
					$args = array(
						'post_type' => 'product',
						'posts_per_page' => 3,
						'orderby' =>'date',
						'order' => 'DESC' );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
						<div class="latest-product_slider owl-carousel ">
							<div class="latest-prdouct_slider_item ">
								<a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<div class="latest-product_item_pic">
										<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="My Image Placeholder" width="65px" height="115px" />'; ?>
									</div>
									<div class="latest-product_item_text">
										<h6><?php the_title(); ?></h6>
										<span><?php echo $product->get_price_html(); ?></span>
									</div>
								</a>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="from-blog ">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="section-title from-blog_title">
					<h2>From The Blog</h2>
				</div>
			</div>
		</div>
		<div class="row">

			<?php 
			$the_query = new WP_Query( array(
				'posts_per_page' => 3,
			)); 
			?>
			<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="blog_item">
							<div class="blog_item_pic">
								<?php echo get_the_post_thumbnail( get_the_ID(), 'full'); ?>
							</div>
							<div class="blog_item_text">
								<ul>
									<li><i class="fa fa-calendar-o"></i> <?php echo get_the_date('Y-m-d', get_the_ID()); ?></li>
									<li><i class="fa fa-comment-o"></i> 5</li>
								</ul>
								<h5><a href="#"><?php the_title(); ?></a></h5>
								<p><?php the_excerpt(); ?></p>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				<?php else : ?>
					<p><?php __('No News'); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<?php
	get_footer();
