<?php
/**
 * Enqueue scripts and styles.
 */
function woocommercecustomtheme_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'woocommercecustomtheme-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'owl-carousel-min', get_template_directory_uri().'/assets/css/owl.carousel.min.css', array(), '1.0.0' );
	wp_enqueue_style( 'owl-theme-default-min', get_template_directory_uri().'/assets/css/owl.theme.default.min.css', array(), '1.0.0' );


	wp_style_add_data( 'woocommercecustomtheme-style', 'rtl', 'replace' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '5.4.1', true);
	wp_enqueue_script( 'bootstrap-slider-js', get_template_directory_uri() . '/assets/js/bootstrap-slider.js', array( 'jquery' ), '11.0.2', true);
	wp_enqueue_script( 'woocommercecustomtheme-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );
	
	wp_enqueue_script( 'owl-carousel-min-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery' ), '1.0.0', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'woocommercecustomtheme_scripts' );
