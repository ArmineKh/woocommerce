<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package woocommerceCustomTheme
 */

get_header();?>
<main id="primary" class="site-main">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-5">
				<?php get_sidebar(); ?>
			</div>

			<div class="col-lg-8 col-md-7">
				<div class="row">
					<?php 
					$the_query = new WP_Query( array(
						'posts_per_page' => 6,
					)); 
					if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div class="col-lg-6 col-md-6" style="margin-bottom: 35px">
								<div class="blog_item">
									<div class="blog_item_pic">
										<?php echo get_the_post_thumbnail( get_the_ID(), 'full'); ?>
										<img src="img/blog-1.jpg" alt="">
									</div>
									<div class="blog_item_text">
										<ul>
											<li><i class="fa fa-calendar-o"></i> <?php echo get_the_date('Y-m-d', get_the_ID()); ?></li>
											<li><i class="fa fa-comment-o"></i> 5</li>
										</ul>
										<h5><a href="#"><?php the_title(); ?></a></h5>
										<p><?php the_excerpt(); ?></p>
										
										<a href="<?php echo the_permalink() ?>" class="blog_btn">READ MORE <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
							</div>
						<?php endwhile; 
						wp_reset_postdata();
						?>
						<div class="col-lg-12">
							<div class="product__pagination blog__pagination">
								<?php 
								echo paginate_links( array(
									'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
									'total'        => $the_query->max_num_pages,
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'format'       => '?paged=%#%',
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
									'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
									'add_args'     => false,
									'add_fragment' => '',
								) );
								?>
							</div>
						</div>

						<?php else : ?>
							<p><?php __('No News'); ?></p>
						<?php endif; ?>
					</div>
				</div>

			</div>
		</div>

	</main><!-- #main -->

	<?php
// get_sidebar();
	get_footer();
