<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package woocommerceCustomTheme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@400;700;900&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
	<script src="https://use.fontawesome.com/3b1d6216fd.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<header id="masthead" class="site-header header">
			<div class="header_top">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="header_top_left">
								<ul>
									<li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
									<li>Free Shipping for all Order of $99</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="header_top_right">
								<div class="header_top_right_social">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-linkedin"></i></a>
									<a href="#"><i class="fa fa-pinterest-p"></i></a>
								</div>
								<div class="header_top_right_language">
									<img src="<?php echo get_template_directory_uri().'/assets/img/language.png' ?>" alt="">
									<div>English</div>
									<span class="arrow_carrot-down"></span>
									<ul class="header_top_right_langs">
										<li><a href="#">Spanis</a></li>
										<li><a href="#">English</a></li>
									</ul>
								</div>
								<div class="header_top_right_auth">
									<a href="#"><i class="fa fa-user"></i> Login</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">

					<div class="col-lg-3">
						<div class="header_logo">
							<a href="<?php echo get_home_url() ?>"><img src="<?php echo get_template_directory_uri().'/assets/img/logo.png' ?>" alt=""></a>
						</div>
					</div>
					<div class="col-lg-6">
						<nav class="navbar navbar-expand-lg navbar-light header_menu">
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
							</button>
							<?php wp_nav_menu(array(
								'menu'       => 'top-menu',
								'menu_class' => 'navbar-nav mr-auto', 
								'depth' => 2,
								'container_id' => 'navbarSupportedContent',
								'container_class' => 'collapse navbar-collapse',
								'items_wrap' => '<ul id="%1$s" class="%2$s" >%3$s</ul>',

								'walker'	 => new Bootstrap_Walker_Nav_Menu()
							));?> 
						</nav>
					</div>
					<div class="col-lg-3">
						<div class="header_cart">
							<ul>
								
								<li>
								<a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" > <i class="fa fa-shopping-bag"></i><span><?php echo WC()->cart->get_cart_contents_count(); ?></span></a>

								</li>
							</ul>
							<div class="header_cart_price">item: <span><?php echo WC()->cart->get_cart_total(); ?></span></div>
						</div>
					</div>
				</div>
			</div>
		</header><!-- #masthead -->
		<section class="hero">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="hero_categories">
							<div class="hero_categories_all">
								<i class="fa fa-bars"></i>
								<span>All departments</span>
								<i class="fas fa-angle-down"></i>
							</div>
							<ul class="hero_cats">
								<?php
								$all_categories = get_all_categories_by_name_for_header();
								foreach ($all_categories as $cat) {
									if($cat->category_parent == 0) {
										$category_id = $cat->term_id;       
										echo '<li><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';
									}       
								}
								?>
							</ul>
						</div>
					</div>
					<div class="col-lg-9">
						<div class="hero_search">
							<div class="hero_search_form">
								<?php 
								echo get_product_search_form();
								?>
							</div>
							<div class="hero_search_phone">
								<div class="hero_search_phone_icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="hero_search_phone_text">
									<h5>+65 11.188.888</h5>
									<span>support 24/7 time</span>
								</div>
							</div>
						</div>
						<div class="hero_item">
							<div class="hero_text">
								<span>FRUIT FRESH</span>
								<h2>Vegetable <br>100% Organic</h2>
								<p>Free Pickup and Delivery Available</p>
								<a href="#" class="primary-btn">SHOP NOW</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
