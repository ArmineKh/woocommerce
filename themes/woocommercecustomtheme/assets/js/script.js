var $ = jQuery.noConflict();
$( document ).ready(function( $ ) {
  $(".dropdown").hover(function() { 
     $(".dropdown-menu").toggleClass("show"); 
 }); 
}); 
$(document).ready(function() { 
  $(".hero_categories_all").click(function() { 
     $(".hero_cats").toggleClass("hide"); 
 }); 
}); 
$(document).ready(function() {

  $(".owl-carousel").owlCarousel({

     items : 4,
     itemsDesktop : [1199,3],
     itemsDesktopSmall : [979,3],
     margin: 40,
     lazyLoad:true,
     nav:true,
     loop:true,
     autoplay:true,
     autoplayTimeout:2000,
     autoplayHoverPause:true

 });

});

$(document).ready(function() {

  $('.latest-product').owlCarousel({
     loop:true,
     nav:true,
     dots: false,
     items: 3,
     autoplay:true,
     autoplayTimeout:5000,

 })

});


$('ul.featured_controls_ul li').click(function(e) {
    var $this = $(this);
    $('ul.featured_controls_ul').find('li.active').last().removeClass('active')
    $this.addClass('active');

    if($this.html() == "Oranges"){
        $('.featured_filter_container div:visible').each(function() { 
    $(this).hide();
});
        // $('.all').hide();
        $('.oranges').show();
    } else if($this.html() == "Fresh Meat"){
        $('.featured_filter_container div:visible').each(function() { 
            $(this).hide();
        });
            // $('.all').hide();
            $('.meat').show();
        } else if($this.html() == "Vegetables"){
            $('.featured_filter_container div:visible').each(function() { 
                $(this).hide();
            });
            // $('.all').hide();
            $('.vegetables').show();
        } else if($this.html() == "Fastfood"){
            $('.featured_filter_container div:visible').each(function() { 
                $(this).hide();
            });
            // $('.all').hide();
            $('.fastfood').show();
        }else if($this.html() == "All"){
            $('.featured_filter_container div:visible').each(function() { 
                $(this).hide();
            });
            $('.all').show();
        }
    });


jQuery( function( $ ) {
    if ( ! String.prototype.getDecimals ) {
        String.prototype.getDecimals = function() {
            var num = this,
            match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if ( ! match ) {
                return 0;
            }
            return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
        }
    }
 // Quantity "plus" and "minus" buttons
 $( document.body ).on( 'click', '.plus, .minus', function() {
    var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
    currentVal  = parseFloat( $qty.val() ),
    max         = parseFloat( $qty.attr( 'max' ) ),
    min         = parseFloat( $qty.attr( 'min' ) ),
    step        = $qty.attr( 'step' );

            // Format values
            if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
            if ( max === '' || max === 'NaN' ) max = '';
            if ( min === '' || min === 'NaN' ) min = 0;
            if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

            // Change the value
            if ( $( this ).is( '.plus' ) ) {
                if ( max && ( currentVal >= max ) ) {
                    $qty.val( max );
                } else {
                    $qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
                }
            } else {
                if ( min && ( currentVal <= min ) ) {
                    $qty.val( min );
                } else if ( currentVal > 0 ) {
                    $qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
                }
            }

            // Trigger change event
            $qty.trigger( 'change' );
        });
});