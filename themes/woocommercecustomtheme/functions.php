<?php
/**
 * woocommerceCustomTheme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package woocommerceCustomTheme
 */


require get_template_directory() . '/includes/theme-settings.php';
require get_template_directory() . '/includes/enqueue-styles-scripts.php';
require get_template_directory() . '/includes/widgets-init.php';
require get_template_directory() . '/includes/navigation-menus.php';

require get_template_directory() . '/woocommerce/includes/wc-functions.php';
require get_template_directory() . '/woocommerce/includes/wc-functions-remove.php';


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


add_filter( 'get_product_search_form' , 'my_custom_product_searchform' );

/**
 * Filter WooCommerce  Search Field
 *
 */
function my_custom_product_searchform( $form ) {
	
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
	<div>
	<div class="hero_search_categories">
	All Categories
	<span class="arrow_carrot-down">
	<i class="fas fa-angle-down"></i>
	</span>
	</div>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __( 'What do you need?', 'woocommerce' ) . '" />
	<input type="submit" class="site-btn search-btn" id="searchsubmit" value="'. esc_attr__( 'Search', 'woocommerce' ) .'" />
	<input type="hidden" name="post_type" value="product" />
	</div>
	</form>';

	return $form;

}




/**
 * Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" > <i class="fa fa-shopping-bag"></i><span><?php echo WC()->cart->get_cart_contents_count(); ?></span></a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
}

add_filter( 'get_search_form', 'my_search_form' );
function my_search_form( $form ) {

	$form = '
	<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >

		<input type="text" value="' . get_search_query() . '" name="s" id="s" 
		placeholder="'. esc_attr_x( 'Search …', 'placeholder' ).'"/>
		<button type="submit"><span class="fa fa-search fa-rotate-90"></span></button>
	</form>';

	return $form;
}




